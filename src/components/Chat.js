import "../App.css";
import { useState, useEffect } from "react";
import Header from "./Header";
import MessageList from "./MessageList";
import Preloader from "./Preloader";

function Chat() {
  const [state, setState] = useState([]);
  const [loading, setLoading] = useState(true);
  const myMessage = {
    id: "80f08600-1b8f-11e8-9629-c7eca82aa7qq",
    userId: "zasibis",
    avatar:
      "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxETERMTExEWFhEVGRUYGBYYFhcZGRgaFxcWHhgXFhYYHiggGBolHhUXIz0hJSsrLi4xFx8zODMsOCgtLisBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAGQAZAMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAAAgQFBgcDAf/EADkQAAEDAgMEBggGAgMAAAAAAAEAAgMEEQUhMRJBUWEGEyJxgZEHMlJiobHR8BQjQnLB4cLxJDOC/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ANxQhJe6yD1zrLntOOmXM/f1XrWXzOvyXRBw/D8STpfhly0HhZIkgZw+JTklcXuQMJWNGgt3Epq6VzfVcRyun8rlH1LR3IFxY05uTwHDiMj5aKWpKxkgu11+I3jvCp9Q6yaMq3RuDmmzh92PEINDQo/B8UbOzaGThk5vA8uSkEAhCEAuQzceA+f38glvNgTwSYG2aP518UHRCElxQJe5NpHLpI5NZXoOUr0wneu8z1HVEiBtWOuFETSJ7UyKImkQP8CxQwztdfsnsuHI/RaWDdYvLItU6MVfW0sTic7WP/nL5WPiglUIQg51Hqnut5pbdAudSOw62tjZKY7sg8ggJHgBN+vBvYg24EH5LOq6BuJ4xU09SXGhoWRAQBzmtllkAO1Js22gMxbkOd0dLeg9NDTy1GHB9JVQtL2mJ7w1+wLlr2EkHK+aDQXTA5b02mes16B+kN1S3q6gAVLBfaGQkaNTbc4bx4q+yVTSLgoEzyJhJdxsP9JVRUt4qs9KekDoBFTwgPq5zZjb5Di553NH8IJCvLW6vz+96hpZRxURi+HUsbC6pcamW13PkJ2Bx6uMHZY3481m2A4o6KrBjuIXvsWXNtlx4cQg1SZ60f0cSXpSOD3fx9Fl0z1p/o0Z/wAQni93yH1QW1CEIPHC4smdNJ2LHVpLT4J6o6p7MnJ4+I/pBQccbJR4m+qY3aiqmMEg96MWyPEAA87lSuJ4tE+ml2XXLmOaG2N+0CNPFTmP4QZIzskE6gHQ+O4qgYhMG5dSTKzJ0IOzKR7UJ/X+35oM1fSup6jabq0gjyzHiLhaxgld1tOx191vLT4WVVxWgjeOtjcXA6hws4ciNQRwKkehjrUtvZkkb5FBM1c9gSVn2F4iJKqqrDo38mHk0es4d/8AkVZ+lVSW00rhqG/0PmqBhuGSNYWFxEQN3cyUDfHquSqdstv1d8z7X9LpheDNjLXuGhB7yFOxUbIwNoEcGD13d4/SE5/Bn/slyI9SMfp5nmgJHk962/opR9VRwsIz2QT3uzt4Xt4LH+i+Hmoq4o7ZXu79rcz8At2aLCyD1CEIBNcRg22G3rDNvePronSEEDDiF22P+uSgsbpIp8pGg20O8dxUt0gpzE7rQPy3nt+67c7uO9Q00iCsV2AyWIZUG3vAOPmQV0wqh6iLq9ouO05xJtmXG50yUpNImM0iBjjFOJYnxkkBwtcajmFDfhJsgZhYbwxod5gZKZmemMz0HGBrIvVF373HM+aaVU5OqXNIpTod0ddWz2NxAwgyO4+40+0fgEF09FWClkTql47UuTP2Df4n5K+pMUYaA1os0AAAaADQBKQCEIQCEIQIljDgWuF2kWIO8FUHHKB9K7O5pnHsP9i/6JDw4OWgrnPC17S17Q5rhYgi4IO4hBmVtrvUJXYPO4ktrpWA6N6mFwHIEi9u9WPpHgMtGeti2n0ozOpfD3+3Hz1G/imkGKQvALjs336tPMEIK0MHnBBdWyv5dTC0HkSBdIrBs96m8SxSJoOx2jx0A8d659Fui0te7rXlzKS+b9HS21bHwbu2vJBH9G+j01dLssu2Fp/MltkPdb7T+W7etpwnDIqeJsUTdljfMneXHeTxXShoo4Y2xxMDI2iwaBkPvinCAQhCAQhCAQhCAQhCDwhU/F+gdI4uewywlxuWxvAZc62Y4EDwQhA1w/0eUbiDI+aQA32XvGybHRwa0XHJXmNgaAGgBoAAAFgANABuCEIFIQhAIQhAIQhB/9k=",
    user: "Dima",
    text: "Йо",
    createdAt: "2021-12-23T13:01:19.922Z",
    editedAt: "",
  };

  useEffect(() => {
    const url = "https://zasibis.github.io/homepage/users.json";
    const fetchData = async () => {
      try {
        const response = await fetch(url);
        const json = await response.json();
        json.unshift(myMessage);
        setState(json.sort((a, b) => a.createdAt.localeCompare(b.createdAt)));
        setLoading(false);
      } catch (error) {
        console.log("error", error);
      }
    };

    fetchData();
  }, []);
  // console.log(state);

  return (
    <div className="chat">
      <Header state={state} />
      {loading && <Preloader />}
      <MessageList state={state} setState={setState} />
    </div>
  );
}

export default Chat;
