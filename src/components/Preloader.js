import React from "react";

const Preloader = () => {
  return (
    <div class="preloader"><div></div></div>
  );
};

export default Preloader;
